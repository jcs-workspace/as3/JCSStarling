package 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.events.Event;

	public class GameObject extends Sprite
	{
		//--- GameObject Status ---//
		protected var currentDisplay:Sprite = null;		// current display (Sprite), in order to check status
		protected var currentAnim:MovieClip = null;		// current display (Animation), in order to check status
		protected var currentDisplayName:String = null;		// make sure the status of the GameObject
		protected var currentDisplayType:int = 1;	 	//1 for texture, 2 for animation
		
		//-- Shake Effect --//
		private var isShake:Boolean = false;		// trigger to active shake effect
		private var shakeOrigin:Point = null;		// store GameObject's position in order to find the right position back after effect
		private var shakeTime:int = 0;			// total time we spend to shake effect
		private var shakeMargin:Number = 0;		// total count of time we shake
		private var shakeTimer : int = 0;		// timer use for shake effect
		
		//-- Alpha Effect --//
		private var isAlpha:Boolean = false;		// trigger to active alpha change
		private var alphaTime:Number = 0;		// total time we want to change our alpha
		private var alphaType:int = 0;		// type of our alpha
		private var alphaTimer:Number = 0;		// timer for alpha change function
		
		//-- Bounce Scale Effect --//
		private var isBounce:Boolean = false;		// trigger active bounce scale effect
		private var bounceTime:Number = 0;		// total time we spend to scale and bounce
		private var bounceMargin:Number = 0;		// count of the bounce and scale effect (what effect actually did)
		private var bounceCycle:Number = 0;		// 
		private var bounceOrigin:Point = null; 		// record origin point in order to find the origin point back
		private var bounceTimer:Number = 0;		// timer use for bounce and scale effect
		private var bounceScaleOrigin:Number = 0;		// 
		
		//-- Scale Effect --//
		private var isScale:Boolean = false;		// trigger active scale effect
		private var scaleTime:Number = 0;		// total time we spend to scale
		private var scaleFinalChange:Number = 0;		// the final change GameObject going to scale
		private var scaleTimer:Number = 0;		// timer use for scale effect
		private var scaleOrigin:Point = null;		// store GameObject's position, in order to find is back after effect alse debugging onShake() function
		
		//-- Move Efftect --//
		private var isMove:Boolean = false;		// trigger active the move effect
		private var moveTime:Number = 0;		// total time we spend to move
		private var movePointTarget:Point = null;		// point we want to shift to
		private var moveTimer:Number = 0;		// timer use for move effect
		
		
		
		public function GameObject()
		{
			addEventListener(Event.ADDED_TO_STAGE,start);
			addEventListener(Event.REMOVED_FROM_STAGE,over);
			
			super();
		}
		
		protected function over(e:Event):void{
			removeEventListener(Event.ADDED_TO_STAGE,start);
			removeEventListener(Event.ENTER_FRAME,update);
			removeEventListener(Event.REMOVED_FROM_STAGE,over);
			this.dispose();
		}
		
		protected function start(e:Event):void{
			addEventListener(Event.ENTER_FRAME,update);
		}
		
		protected function update(e:Event):void{
			onShake();		//enable shake function
			onAlphaChange();		//enable alpha change function
			onScaleBounce();		//enable scale bounce
			onScaleChange();		//enable scale change
			onMoveTo();		//enable moveto function
		}
		
		/**
		 * Effect
		 * this function use to set shake attribute and start a shake effect
		 * last: how much time will this shake
		 * margin: how intense the shake willbe
		 */	
		public function shake(time:int, margin:Number){
			if(isShake){		// debug for shak effect
				trace("trying to call a shake effect while there is already one");
			}else{
				isShake = true;
				shakeTime = time;
				shakeMargin = margin;
				shakeOrigin = new Point(this.x, this.y);
			}
		}
		
		/**
		 * this will do actualy shake according to the shake attribute
		 */
		protected function onShake(){
			// start shaking
			if(isShake){
				shakeTimer++;		// while shaking run timer!!
				// still on shake
				if(shakeTimer < shakeTime){
					// start from origin
					x = shakeOrigin.x;
					y = shakeOrigin.y;
					// shake randomly
					this.x += (Math.random()*2 - 1) * shakeMargin * (shakeTime/shakeTimer)/5;		// shakeTime / shakeTimer = shakeRate
					this.y += (Math.random()*2 - 1) * shakeMargin * (shakeTime/ shakeTimer)/5;		
				}
				else{//finish the shake
					// back to original position
					x = shakeOrigin.x;
					y = shakeOrigin.y;
					
					shakeTimer = 0; // reset timer back to 0, wait for next shake
					isShake = false;	// reset Boolean
				}
			}// end shake
		}
		
		/**
		 * Effect
		 * this gameobject from transparent to full alpha (appear)
		 * or from full alpha to transparent (disappear),
		 * type 0: disappear,
		 * type 1: appear,
		 */
		public function alphaChange(time:int, type:int){
			// debug for alpha change
			if(isAlpha){
				trace("trying to call a alpha change effect while there is already one");
			}else{
				if(type == 0){		// set up disapper effect, alpha 1 -> 0
					isAlpha = true;
					alphaTime = time;
					//reset alpha to current and right alpha
					alpha = 1;
					alphaType = type;
				}else if(type == 1){	// set up appear effect, alpha 0 -> 1
					isAlpha = true;
					alphaTime = time;
					//reset alpha to current and right alpha
					alpha = 0;
					alphaType = type;
				}
					
			}
		}
		
		/**
		 * function actually doing alphaChange 
		 */
		protected function onAlphaChange(){
			if(isAlpha){
				// do alpha change
				if(alphaTimer < alphaTime){
					if(alphaType == 0) alpha -= 1/alphaTime;
					else if(alphaType == 1) alpha += 1/alphaTime; 
				}
				else{		// else we do this in normal
					// check alpha
					if(alphaType == 0) this.alpha = 0;
					else if (alphaType == 1) this.alpha = 1;
					
					alphaTimer = 0;
					isAlpha = false;
				}
				alphaTimer++;
			}// end if(isAlpha)
		}
		
		
		/**
		 * Effect
		 * Scale Bounce,
		 * time: effect time length,
		 * margin: how big the bounce is,
		 * cycle: how many time will bounce
		 */
		public function scaleBounce(time:int, margin:Number, cycle:Number){
			if(isBounce){
				trace("trying to call a bounce effect while there is already one");
			}else{
				bounceTime = time;
				bounceMargin = margin;
				bounceCycle = cycle;
				isBounce = true;
				//set oringin position
				bounceOrigin = new Point(this.x, this.y);		// find the point position automatically
				//record oringin scale
				bounceScaleOrigin = this.scaleX;
			}
		}
		
		protected function onScaleBounce(){
			if(isBounce){
				bounceTimer++;
				
				// do bounce effect
				if(bounceTimer <= bounceTime){
					// scale formula
					var scale : Number = Math.sin(	2 * Math.PI *	// 2 PI, one cycle
													bounceCycle *	// how many cycle
													bounceTimer/bounceTime) * 		// (speed/time) while bouncing
													bounceMargin/100 *		//set margin
													(-0.5 * bounceTimer + bounceTime * 0.5)		//line boucing decline
													+ 1;		//scaling from oringin scale
					
					//if is not shaking stay in center
					if(!isShake){
						x = bounceOrigin.x - (scale-1)*(width/scaleX)/2;
						y = bounceOrigin.y - (scale-1)*(width/scaleY)/2;
					}
					scaleX = scaleY = scale;
				}
				else{		// end scale bounce effect
					//reset scale
					scaleX = scaleY = bounceScaleOrigin;
					bounceTimer = 0;
					isBounce = false;		// when if(isBounce) run more than twice turn this to false, break the function!
				}
			}// end if(isBounce)
		}
		
		/**
		 * Effect
		 * smoothly scale to target scale in given time peroid
		 */
		public function scaleChange(time:int, finalChange:Number){
			if(isScale){
				trace("trying to call a scalechange effect while there is already one");
			}else{
				isScale = true;
				scaleTime = time;
				scaleFinalChange = finalChange;
				scaleOrigin = new Point(this.x,this.y);
			}
		}
		
		protected function onScaleChange(){
			if(isScale){
				scaleTimer++;
				if(scaleTimer < scaleTime){
					// change height and with to final change
					scaleX += (scaleFinalChange - scaleX) / scaleTime;
					scaleY += (scaleFinalChange - scaleY) / scaleTime;
					
					if(!isShake){
						x = scaleOrigin.x - (scaleX-1)*(width/scaleX)/2;
						y = scaleOrigin.y - (scaleY-1)*(width/scaleY)/2;
					}
				}
				else{
					isScale = false;
				}
			}// end if(isScale)
		}
		
		/**
		 * Effect Move:
		 * time: length of effect time.
		 * shift: point we shift
		 */
		public function moveTo(time:int, shift: Point){
			if(isMove){ 
				trace("already in move to function");
			}else{
				isMove = true;
				moveTime = time;
				movePointTarget = shift;
			}
		}
		
		/**
		 * move to function
		 */
		protected function onMoveTo(){
			if(isMove){
				moveTimer++;
				
				if(moveTimer < moveTime){
					this.x += movePointTarget.x/moveTime;
					this.y += movePointTarget.y/moveTime;
				}else{
					
					moveTime = 0;
					isMove = false;
				}
			}
		}
		
		/**
		 * will change current Display, search by name.
		 * add to given point p,
		 * type:: 1 for image, 2 for animation
		 * assetsType:: 1 for character, 2 for effects, 3 for items, 4 for map,
		 * 				5 for Mob, 6 for skills, 7 for strings, 8 for UI,
		 * fr:: FrameRate for each animation = 14 for default
		 * career: 1 for Warrior, 2 for Wizard, 3 for Archer, 4 for Thief
		 * Point: position,
		 * isLoop: true play forever, false play once,
		 */
		public function changeDisplay(name:String, type:int = 1, assetsType: int = 1, fr:int = 14, career:int = 1, p:Point = null, isLoop:Boolean = true):void{
			// create display sprite if null
			if(currentDisplay == null){
				currentDisplayName = null;
				currentDisplay = new Sprite();
			}
			
			// if is the display object, no need to add new display
			if(name != currentDisplayName || type != currentDisplayType){
				// set properties
				currentDisplayName = name;
				
				//start chage to new display
				//remove old and create new
				removeChild(currentDisplay);
				currentDisplay = new Sprite();
				if(p != null){
					currentDisplay.x = p.x;
					currentDisplay.y = p.y;
				}
				addChild(currentDisplay);
				
				if(type == 1){		// sprite
					var img: Image;
					
					if(assetsType == 1) img = Main.characters_assets.findImage(name);
					else if(assetsType == 2) img = Main.effects_assets.findImage(name);
					else if(assetsType == 3) img = Main.items_assets.findImage(name);
					else if(assetsType == 4) img = Main.map_assets.findImage(name, 1);
					else if(assetsType == 5) img = Main.mob_assets.findImage(name);
					else if(assetsType == 6) img = Main.skills_assets.findImage(name);
					else if(assetsType == 7) img = Main.strings_assets.findImage(name);
					else if(assetsType == 8) img = Main.ui_assets.findImage(name);
					
					
					if(img != null){
						//add on currentDisplay
						currentDisplay.addChild(img);
					}else trace(name, "not found when trying to change display texture");
					
					currentDisplayType = 1;		// after we are done with texturing set diplayType to 1
				}else if(type == 2){
					var mc:MovieClip;
					
					if(assetsType == 1) mc = Main.characters_assets.findAnimation(name,fr, career);
					else if(assetsType == 2) mc = Main.effects_assets.findAnimation(name , fr);
					else if(assetsType == 3) mc = Main.items_assets.findAnimation(name,fr);
					else if(assetsType == 4) mc = Main.map_assets.findAnimation(name);
					else if(assetsType == 5) mc = Main.mob_assets.findAnimation(name, fr);
					else if(assetsType == 6) mc = Main.skills_assets.findAnimation(name, fr);
					else if(assetsType == 7) mc = Main.strings_assets.findAnimation(name, fr);
					else if(assetsType == 8) mc = Main.ui_assets.findAnimation(name);
					else trace("(Class: GameObject)Unkwon assets type: ", assetsType);		// display unkwon assets type
					
					if(mc != null){
						// set loop
						mc.loop = isLoop;
						// add an currentDisplay
						currentAnim = mc;
						currentDisplay.addChild(mc);
						Starling2DFramework_JCS._starling.juggler.add(mc);
					}else trace(name,"not found when trying to change display animation");
					
					currentDisplayType = 2;		// after we are done with texturing set diplayType to 2
				}
			}
			
		}
		
		/**
		 * find Animation when object are global
		 * name : xml inside name
		 * type: 1 for image, 2 for animation
		 * assetType: which assets type
		 * fr: Frame_Rate
		 * isLoop : (only animation) loop animation or not
		 */
		public function displayGlobalObject(name:String, type:int= 1, assetsType:int = 1, fr:Number = 1, p:Point = null,isLoop:Boolean = true):void{
			// create display sprite if null
			if(currentDisplay == null){
				currentDisplayName = null;
				currentDisplay = new Sprite();
			}
			
			// if is the display object, no need to add new display
			if(name != currentDisplayName || type != currentDisplayType){
				// set properties
				currentDisplayName = name;
				
				//start chage to new display
				//remove old and create new
				removeChild(currentDisplay);
				currentDisplay = new Sprite();
				if(p != null){
					currentDisplay.x = p.x;
					currentDisplay.y = p.y;
				}
				addChild(currentDisplay);
				
				if(type == 1){ // image
					
				}else if(type == 2){ // animationed object
					var mc :MovieClip;
					if(assetsType == 4) mc = Main.map_assets.findAnimationFormGlobal(name,fr);
					
					if(mc != null){
						// set loop
						mc.loop = isLoop;
						// add an currentDisplay
						currentDisplay.addChild(mc);
						Starling2DFramework_JCS._starling.juggler.add(mc);
					}else trace(name,"not found when trying to change display animation");
					
					currentDisplayType = 2;
				}
			}
		}// end function
		
		/**
		 * change display facing direction from current to new:
		 * -1 : facing left
		 * 1: facing right
		 */
		public function changeDisplayDirection(direction:int){
			// need to facing right but not
			if(direction == -1 && currentDisplay.scaleX != 1){
				currentDisplay.scaleX = 1;
				currentDisplay.x -= currentDisplay.width;
			}// need to facing left but not
			else if(direction == 1 && currentDisplay.scaleX != -1){
				currentDisplay.scaleX = -1;
				currentDisplay.x += currentDisplay.width;
			}
		}
		
		/**
		 * This will adjust the texture that does not compatible with the engine
		 * x position
		 */
		public function changeDisplayPositionX(xPosition:Number = 0){
			currentDisplay.x = xPosition;
		}
		
		/**
		 * This will adjust the texture that does not compatible with the engine
		 * y position
		 */
		public function changeDisplayPositionY(yPosition:Number = 0){
			currentDisplay.y = yPosition;
		}
		
		/**
		 * use rectangle colide with target object
		 * if collide return true
		 * else return false
		 */
		protected function rectCollision(origin:Rectangle, object:DisplayObject):Boolean{
			if(origin.x + origin.width > object.x && origin.x < object.x + object.width &&
				origin.y + origin.height > object.y && origin.y < object.y + object.height)
				return true;
			else
				return false;
		}
		
		/**
		 * use self colide with target object
		 * if collide return true
		 * else return false
		 */
		protected function colideWith(object:DisplayObject):Boolean{
			if(this.x+this.width > object.x && this.x < object.x + object.width &&
				this.y+this.height > object.y && this.y < object.y + object.height)
				return true;
			else
				return false;
		}
		
		/**
		 * check if local point p colide with target object
		 * return true if collide
		 * false if not
		 */
		protected function pointCollision(p:Point, object:DisplayObject):Boolean{
			if(p.x > object.x && p.x < object.x + object.width &&
				p.y > object.y && p.y < object.y + object.height){
				return true;
			}else{
				return false;
			}
		}
		
		/**
		 * check if target object collide with a circle at p point with given radius r
		 */
		protected function radiusCollision(p:Point, object:DisplayObject, r: Number):Boolean{
			//find the center of the target object
			var targetCenter : Point = new Point(object.x+object.width/2, object.y+object.height/2);
			
			//get distance from p to target object.
			var distance:Point = new Point(targetCenter.x-p.x, targetCenter.y-p.y);
			
			//get distance of where circle and object meet
			var angel:Number = Math.atan2(distance.y, distance.x);
			var mx:Number = Math.cos(angel)*r + p.x;
			var my:Number = Math.sin(angel)*r + p.y;
			var detectPoint:Point = new Point(mx, my);
			
			//trace("a radius collision hace been called, detectPoint:",detectPoint,"radius:",r);
			return pointCollision(detectPoint,object);
		}
		
		
		/**
		 * this function will contain all the effect player need,
		 * in order to, manage if the path changes!
		 * Function called need to contain thier own condition.
		 */
		protected function pPlaySoundEffect(type:int):void{
			if(type == 0) Main.audioManager.playsound("/Characters/jump");	// jump
			else if(type == 1) Main.audioManager.playsound("/Characters/portal");	// transfer
		}
		
	}
}






