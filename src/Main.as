package 
{
	import flash.display.StageDisplayState;
	import flash.geom.Rectangle;
	import flash.media.SoundChannel;
	import flash.ui.Keyboard;
	
	import assets.Characters_Assets;
	import assets.Effects_Assets;
	import assets.Items_Assets;
	import assets.Map_Assets;
	import assets.Mob_Assets;
	import assets.Skills_Assets;
	import assets.Strings_Assets;
	import assets.UI_Assets;
	
	import interfaces.Interface;
	import interfaces.Login_Interface;
	import interfaces.SelectCharacter_Interface;
	import interfaces.World_Interface;
	import interfaces.map.Novice_Market;
	import interfaces.map.Novice_Road_01;
	import interfaces.map.Novice_Village;
	
	import managers.AudioManager;
	import managers.Game_Manager;
	import managers.Map_Manager;
	
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	
	public class Main extends Sprite
	{
		public static const DEVELOPER_USED:Boolean = true;
		
		private static const RESET_TIME:int = 60 * 60;
		private static const SWITCH_INTERFACE_TIME:int = 120;
		
		public static var instance :Main = null;
		//-- Assets Setup --////////////////////////////////////////////////////////////////
		public static var assets : Assets = null;//////////////////////////////////////////0
		public static var characters_assets : Characters_Assets = null;		// Characters: 1
		public static var effects_assets : Effects_Assets = null;			//   Effects:  2
		public static var items_assets : Items_Assets = null;				//   Items:	   3
		public static var map_assets:Map_Assets = null;						//    Map:     4
		public static var mob_assets : Mob_Assets = null;					//   Mob:      5
		public static var skills_assets : Skills_Assets = null;				//   Skills:   6
		public static var strings_assets : Strings_Assets = null;			//  Strings:   7
		public static var ui_assets:UI_Assets = null;						//    UI:      8
		
		//--- Global Managers ---//
		public static var game_manager:Game_Manager = null;
		public static var audioManager:AudioManager = null;
		public static var map_manager:Map_Manager = null
		
		public static var frameRates : Number = 60;
		
		//these record size of window, use to compare with current window to call resizeWindow()
		private var currentWindowWidth:int = 0;
		private var currentWindowHeight:int = 0;
		
		//-- Current Interfadce's properties
		public static var currentClientCharacter:GameObject = null;		// after player decide which character he want to play
		public var currentInterface:Interface = null;		// find what interface/map currentClientCharacter are
		public var current_MapMaxWidth :Number = 0;			// find out what current map width are (Camera movenment use)
		public var current_MapMinWidth :Number = 0;
		public var current_MapMaxHeight :Number = 0;			// find out what current map height are (Camera movenment use)
		public var current_MapMinHeight :Number = 0;
		public var currentInterfaceName:String = "Login_Interface";
		private var newInterfaceName:String = "null";
		
		//-- Current client  Character's career
		public var playerChoice:int = 0;		// 0: none, 1: warrior, 2: wizard, 3: archer, 4: thief
		public var characterCareer1 :String = "warrior";
		public var characterCareer2 :String = "wizard";
		public var characterCareer3 :String = "archer";
		public var characterCareer4 :String = "thief";
		
		private var interfaceTimer : int = 0;
		private var isSwitchInterface : Boolean = false;
		private var resetTimer : int = 0;
		private var isReset:Boolean = true;
		
		public var bgm:SoundChannel = null;		// channel play music
		
		public function Main()
		{
			instance = this;
			
			//assets = new Assets();		// assign asset to  use
			characters_assets = new Characters_Assets();
			map_assets = new Map_Assets();
			ui_assets = new UI_Assets();
			
			game_manager = new Game_Manager();
			audioManager = new AudioManager();		// assign audio to use
			map_manager = new Map_Manager();
			
			addEventListener(Event.ADDED_TO_STAGE,start);
			addEventListener(Event.REMOVED_FROM_STAGE,over);
			addEventListener(Event.ENTER_FRAME,update);
			
			super();
		}
		
		private function over(e:Event):void{
			removeEventListener(Event.ADDED_TO_STAGE,start);
			removeEventListener(KeyboardEvent.KEY_UP,keyreleased);
			removeEventListener(KeyboardEvent.KEY_UP,keypressed);
			removeEventListener(Event.REMOVED_FROM_STAGE,over);
		}
		
		private function start(e:Event):void{
			addEventListener(KeyboardEvent.KEY_UP,keypressed);
			addEventListener(KeyboardEvent.KEY_UP,keyreleased);
			
			// we start with the login scene
			currentInterface = new Login_Interface();
			addChild(currentInterface);
			
			//record default window size
			currentWindowWidth = Starling2DFramework_JCS._starling.nativeStage.stageWidth;
			currentWindowHeight = Starling2DFramework_JCS._starling.nativeStage.stageHeight
				
			//resizeWindow Once
			resizeWindow();
		}
		
		private function update(e:Event):void{
			//resize window if player change resolution
			if(Starling2DFramework_JCS._starling.nativeStage.stageWidth != currentWindowWidth ||
				Starling2DFramework_JCS._starling.nativeStage.stageHeight != currentWindowHeight)
				resizeWindow();
			
			//enable scene switch alpha function
			onInterfaceSwitch();
		}
		
		/**
		 * check interface name or map name
		 */
		public function checkInterface(interfaceName: String):void{
			if(interfaceName == "Login_Interface") {
				playerChoice = 0;	// set character career back to 0 (none selected!)
				currentInterface = new Login_Interface();
			}else if(interfaceName == "SelectCharacter_Interface") currentInterface = new SelectCharacter_Interface();
			else if(interfaceName == "World_Interface") currentInterface = new World_Interface();
			// check map
			else if(interfaceName == "Novice_Village") currentInterface = new Novice_Village(playerChoice);
			else if(interfaceName == "Novice_Road_01") currentInterface = new Novice_Road_01(playerChoice);
			else if(interfaceName == "Novice_Market") currentInterface = new Novice_Market(playerChoice);
		}
		
		/**
		 * actual function for scene switch
		 */
		private function onInterfaceSwitch():void{
			if(isSwitchInterface){
				interfaceTimer++;
				
				//first half of switch, deleting current scene
				if(interfaceTimer < SWITCH_INTERFACE_TIME/2)	currentInterface.alpha -= 1/(SWITCH_INTERFACE_TIME/4);
				//second half of switch, creating new scene
				else if(interfaceTimer > SWITCH_INTERFACE_TIME/2) currentInterface.alpha += 1/(SWITCH_INTERFACE_TIME/4);
				
				//middle of the switch, end deleting currentlevel
				if(interfaceTimer == SWITCH_INTERFACE_TIME/2){
					removeChild(currentInterface);		// remove current interface
					currentInterface = null;		// set current interface equals to null
					
					//according to new interface name, addchild new interface
					checkInterface(newInterfaceName);
					
					
					game_manager.mGameStatus = newInterfaceName;
					//add on stage and set alpha to 0
					addChild(currentInterface);
					currentInterface.alpha = 0;
				}
				
				//end of the switch interface
				if(interfaceTimer == SWITCH_INTERFACE_TIME){
					currentInterface.alpha = 1;
					//reset timer
					interfaceTimer = 0;
					//over switch
					isSwitchInterface = false;
					//record current scene
					currentInterfaceName = newInterfaceName; 
				}
			}
		}
		
		/**
		 * switch to given scene: Game/Title
		 */
		public function switchInterface(name:String):void{
			if(name != currentInterfaceName){
				trace("Main: Switching to new interface/map:", name);
				// assign interface to global, in order to let "onSwitchInterface" function caa switch interface!!
				newInterfaceName = name;
				
				isSwitchInterface = true; 	//activate scene switch
			}
		}
		
		/**
		 * this function will keep stage fit the window size when player resizing game window
		 */
		private function resizeWindow():void{
			var windowWidth:int = Starling2DFramework_JCS._starling.nativeStage.stageWidth
			var windowHeight:int = Starling2DFramework_JCS._starling.nativeStage.stageHeight;
			var newWindowWidth:int = 0;
			var newWindowHeight:int = 0;
			
			//screen is wider than default portion
			if(windowWidth / windowHeight > Starling2DFramework_JCS.SCREEN_WIDTH/Starling2DFramework_JCS.SCREEN_HEIGHT){
				newWindowHeight = windowHeight;
				newWindowWidth = newWindowHeight * Starling2DFramework_JCS.SCREEN_WIDTH/Starling2DFramework_JCS.SCREEN_HEIGHT;
				Starling2DFramework_JCS._starling.viewPort = new Rectangle((windowWidth-newWindowWidth)/2,0,newWindowWidth,newWindowHeight);
			}else{		// screen is taller than default portion
				newWindowWidth = windowWidth;
				newWindowHeight = newWindowWidth * Starling2DFramework_JCS.SCREEN_HEIGHT/Starling2DFramework_JCS.SCREEN_WIDTH;
				Starling2DFramework_JCS._starling.viewPort = new Rectangle(0,(windowHeight-newWindowHeight)/2,newWindowWidth,newWindowHeight);
			}
			
			trace("window resized");
			//update current window size
			currentWindowWidth = Starling2DFramework_JCS._starling.nativeStage.stageWidth;
			currentWindowHeight = Starling2DFramework_JCS._starling.nativeStage.stageHeight;
		}
		
		private function keyreleased(e:KeyboardEvent):void{
			
		}
		
		private function keypressed(e:KeyboardEvent):void{
			if(e.keyCode == Keyboard.F10) fullScreen();
		}
		
		/**
		 * switch between fullscreen mode and windowed mode
		 */
		public function fullScreen():void{
			if(Starling2DFramework_JCS._starling.nativeStage.displayState == StageDisplayState.FULL_SCREEN_INTERACTIVE)
				Starling2DFramework_JCS._starling.nativeStage.displayState = StageDisplayState.NORMAL;
			else
				Starling2DFramework_JCS._starling.nativeStage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
		}
		/**
		 * 
		 */
		public function setBGM(bgmName:String, volume:Number = 1):void{
			// not play any musice
			if(bgm == null){		// debug music
				bgm = audioManager.playsound(bgmName, volume, int.MAX_VALUE);
			}else{
				bgm.stop();
				bgm = audioManager.playsound(bgmName, volume, int.MAX_VALUE);
			}
		}
		
	}
}