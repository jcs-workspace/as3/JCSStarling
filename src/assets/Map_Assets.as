package assets
{
	import flash.display.Bitmap;
	
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	

	public class Map_Assets extends Assets
	{
		/**--- Globa Object ---*/
		[Embed(source="../data/sprites/Map/Global/portal_01.png")]
		public static var portalAnimPNG : Class;
		[Embed(source="../data/xml/Map/Global/portal_01.xml",mimeType="application/octet-stream")]
		public static var portalAnimXML : Class;
		public static var portalObject:TextureAtlas;		// the actual animation apply to GameObject
		
//------------------- Sprite ----------------------//
		//-- Novie_Village --//
		[Embed(source="../data/sprites/Map/Novice_Village/Novice_Village_Sky.png")]////////////////////////////////-- SKY
		public static var Novice_Village_Sky : Class;
		private var novice_village_skyImage:Image = null;		// this image is too big so we do not need declare his own texture atlas
		[Embed(source="../data/sprites/Map/Novice_Village/GAndP.png")]
		public static var novice_villagePNG: Class;
		[Embed(source="../data/xml/Map/Novice_Village/GAndP.xml",mimeType="application/octet-stream")]
		public static var novice_villageXML : Class;
		public static var novice_villageObject :TextureAtlas;		// this image have more than one texture
		
		//-- Novice_Road_01 --//
		[Embed(source="../data/sprites/Map/Novice_Road_01/Novice_Road_01_Sky.png")]////////////////////////////////-- Sky
		public static var novice_road_01_sky : Class;
		private var novice_road_01_skyImage:Image = null;		// this image is too big so we do not need declare his own texture atlas
		
		//-- Novice_Market--//
		[Embed(source="../data/sprites/Map/Novice_Market/Novice_Market_Sky.png")]////////////////////////////////-- Sky
		public static var novice_market_sky : Class;
		private var novice_market_skyImage:Image = null;		// this image is too big so we do not need declare his own texture atlas
		
//---------------- Animation ----------------------//
		
		
		
		public function Map_Assets()
		{
			mapGlobal();
			mapImage();
			mapAnimation();
			
			super();
		}
		
		/**
		 * Map Assets
		 */
		/** Global Assets */
		private function mapGlobal():void{
			//create animation atlas
			var portalBitmap:Bitmap = new portalAnimPNG();
			var portalText :Texture = Texture.fromBitmap(portalBitmap);
			var portalXML :XML = XML(new portalAnimXML);
			portalObject = new TextureAtlas(portalText,portalXML);
		}
		
		private function mapImage():void{
			novice_village_skyImage = Image.fromBitmap(new Novice_Village_Sky());	//-- Novice_Village --//
			var bitmap:Bitmap = new novice_villagePNG();
			var texture :Texture = Texture.fromBitmap(bitmap);
			var xml :XML = XML(new novice_villageXML);
			novice_villageObject = new TextureAtlas(texture,xml);
			
			novice_road_01_skyImage = Image.fromBitmap(new novice_road_01_sky());   //-- Novice_Road_01 --//
			
			novice_market_skyImage = Image.fromBitmap(new novice_market_sky());   //-- Novice_Market --//
		}
		
		private function mapAnimation():void{
			
		}
		
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/**
		 * use to check large image
		 */
		private function checkLargeImageType(pngName:String):Image{
			if(pngName == "Novice_Village_Sky"){
				return Image.fromBitmap(new Novice_Village_Sky());
			}else if(pngName == "Novice_Road_01_Sly"){
				return Image.fromBitmap(new novice_road_01_sky());
			}else if(pngName == "Novice_Market_Sky"){
				return Image.fromBitmap(new novice_market_sky());
			}
			return null;
		}
		
		/**
		 * return a Image by name
		 * name: Name of the Image in XML file,
		 * type: 1 for normal image, 2 for large image
		 */
		public function findImage(name:String, type:int):Image{
			if(type == 2){				// find larg image
				if(name != null){
					return checkLargeImageType(name);
				}else{
					trace("failed to create largeImage:", name);
					return null;
				}
			}else if(type == 1){		// find map's object
				if(findTexture(name) == null){
					trace("failed to create Image :",name);
					return null;
				}
			}
			
			return new Image(findTexture(name));
		}
		
		/**
		 * return a texture by name
		 */
		public function findTexture(name:String):Texture{
			if(novice_villageObject.getTexture(name) != null){
				return novice_villageObject.getTexture(name);
			}else return null;
		}
		
		/**
		 * return a MovieClip found in texture by name
		 * name: name in XML file
		 */
		public function findAnimation(name:String):MovieClip{
			var runframes : Vector.<Texture> = Assets.playerPNG.getTextures(name);
			if(runframes == null) return null;
			
			var anim:MovieClip = new MovieClip(runframes,animation_frame_rate);
			return anim;
		}
		
		/**
		 * return a MovieClip from Global Assets
		 */
		public function findAnimationFormGlobal(name:String, fr:Number = 1):MovieClip{
			var runframes : Vector.<Texture> = Map_Assets.portalObject.getTextures(name);
			if(runframes == null) return null;
			
			var anim:MovieClip = new MovieClip(runframes,fr);
			return anim;
		}
		
	}
}