package assets
{
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.textures.Texture;

	public class Items_Assets extends Assets
	{
//------------------- Sprite ----------------------//
		
		
//---------------- Animation ----------------------//
		
		
		
		public function Items_Assets()
		{
			fGlobal();
			fItemsImage();
			fItemsAnimation();
			super();
		}
		
		private function fGlobal():void{
			
		}
		
		private function fItemsImage():void{
			
		}
		
		private function fItemsAnimation():void{
			
		}
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/**
		 * use to check large image
		 */
		private function checkLargeImageType(pngName:String):Image{
			
			return null;
		}
		
		/**
		 * return a Image by name
		 */
		public function findImage(name:String):Image{
			
			return null
		}
		
		/**
		 * return a texture by name
		 */
		public function findTexture(name:String):Texture{
			
			return null;
		}
		
		/**
		 * return a MovieClip found in texture by name
		 */
		public function findAnimation(name:String, frameRate:int):MovieClip{
			var runframes : Vector.<Texture> = null; 
			if(runframes == null) return null;
			
			var anim:MovieClip = new MovieClip(runframes,frameRate);
			return anim;
		}
		
		
		
	}// end class
}// end package