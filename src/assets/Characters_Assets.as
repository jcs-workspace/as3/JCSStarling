package assets
{
	import flash.display.Bitmap;
	
	import starling.core.starling_internal;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	public class Characters_Assets extends Assets
	{
//------------------- Sprite ----------------------//
		[Embed(source="../data/sprites/UI/login_interface.png")]
		public static var logo : Class;
		[Embed(source="../data/xml/UI/login_interface.xml",mimeType="application/octet-stream")]
		public static var logoXML : Class;
		public static var logoPNG :TextureAtlas;		// this image have more than one texture
		
		
//---------------- Animation ----------------------//
		[Embed(source="../data/sprites/characters/warrior.png")]
		public static var warriorAnimPNG : Class;
		[Embed(source="../data/xml/characters/warrior.xml",mimeType="application/octet-stream")]
		public static var warriorAnimXML : Class;
		public static var warriorObject:TextureAtlas;		// the actual animation apply to GameObject
		
		[Embed(source="../data/sprites/characters/thief.png")]
		public static var thiefAnimPNG : Class;
		[Embed(source="../data/xml/characters/thief.xml",mimeType="application/octet-stream")]
		public static var thiefAnimXML : Class;
		public static var thiefObject:TextureAtlas;		// the actual animation apply to GameObject
		
		
		public function Characters_Assets()
		{
			characterImage()
			characterAnimation();
			
			super();
		}
		
		/**
		 * Character Assets
		 */
		private function characterImage():void{
			
		}
		
		private function characterAnimation():void{
			//create animation atlas
			var warriorBitmap:Bitmap = new warriorAnimPNG();
			var warriorText :Texture = Texture.fromBitmap(warriorBitmap);
			var warriorXML :XML = XML(new warriorAnimXML);
			warriorObject = new TextureAtlas(warriorText,warriorXML);
			
			var thiefBitmap:Bitmap = new thiefAnimPNG();
			var thiefText :Texture = Texture.fromBitmap(thiefBitmap);
			var thiefXML :XML = XML(new thiefAnimXML);
			thiefObject = new TextureAtlas(thiefText,thiefXML);
		}
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/**
		 * return a Image by name
		 */
		public function findImage(name:String):Image{
			if(name == "sky_Test"){
				return null;
			}
			else if(findTexture(name) == null){
				return null;
			}
			return new Image(findTexture(name));
		}
		
		/**
		 * return a texture by name
		 */
		public function findTexture(name:String):Texture{
			if(logoPNG.getTexture(name) != null){
				return logoPNG.getTexture(name);
			}else return null;
		}
		
		/**
		 * return a MovieClip found in texture by name
		 */
		public function findAnimation(name:String, frameRate:int, career:int):MovieClip{
			var runframes : Vector.<Texture> = null;
			if(career == 1) runframes = Characters_Assets.warriorObject.getTextures(name);		// warrior
			//else if(career == 2) runframes = Characters_Assets.warriorObject.getTextures(name);	  // wizard
			//else if(career == 3) runframes = Characters_Assets.warriorObject.getTextures(name);   // archer
			else if(career == 4) runframes = Characters_Assets.thiefObject.getTextures(name);	    // thief
			if(runframes == null) return null;
			
			if(frameRate <= 0) trace("FrameRate could not lower or equals to Zero: ", name);
			else var anim:MovieClip = new MovieClip(runframes,frameRate);
			
			return anim;
		}
		
	}// end Class
}// end Package