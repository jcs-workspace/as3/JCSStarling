package assets
{
	import flash.display.Bitmap;
	
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	public class UI_Assets extends Assets
	{
//------------------- Sprite ----------------------//
		//-- Logo --//
		[Embed(source="../data/sprites/UI/login_interface.png")]
		public static var login_interfacePNG : Class;
		[Embed(source="../data/xml/UI/login_interface.xml",mimeType="application/octet-stream")]
		public static var login_interfaceXML : Class;
		public static var login_interfaceObj :TextureAtlas;		// this image have more than one texture
		
//---------------- Animation ----------------------//3
		[Embed(source="../data/sprites/characters/player.png")]
		public static var animDefault : Class;
		[Embed(source="../data/xml/characters/player.xml",mimeType="application/octet-stream")]
		public static var animDefaultXML : Class;
		
		
		
		public function UI_Assets()
		{
			uiImage();
			uiAnimation();
			
			super();
		}
		
		/**
		 * UI Image Assets
		 */
		public function uiImage():void{
			var bitmap:Bitmap = new login_interfacePNG();			//-- Logo --//
			var texture :Texture = Texture.fromBitmap(bitmap);
			var xml :XML = XML(new login_interfaceXML);
			login_interfaceObj = new TextureAtlas(texture,xml);
			
			
		}
		
		/**
		 * UI Animation Assets
		 */
		public function uiAnimation():void{
			//create animation atlas
			var defaultBitmap:Bitmap = new animDefault();
			var defaultTex :Texture = Texture.fromBitmap(defaultBitmap);
			var defaultXML :XML = XML(new animDefaultXML);
			animDefaultPNG = new TextureAtlas(defaultTex,defaultXML);
		}
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/**
		 * return a Image by name
		 */
		public function findImage(name:String):Image{
			if(name == "sky_Test"){
				trace("This should for large image: UI_Assets");
			}
			else if(findTexture(name) == null){
				return null;
			}
			return new Image(findTexture(name));
		}
		
		/**
		 * return a texture by name
		 */
		public function findTexture(name:String):Texture{
			if(login_interfaceObj.getTexture(name) != null){
				return login_interfaceObj.getTexture(name);
			}else return null;
		}
		
		/**
		 * return a MovieClip found in texture by name
		 */
		public function findAnimation(name:String):MovieClip{
			var runframes : Vector.<Texture> = Assets.animDefaultPNG.getTextures(name);
			if(runframes == null) return null;
			
			var anim:MovieClip = new MovieClip(runframes,animation_frame_rate);
			return anim;
		}
		
	}
}