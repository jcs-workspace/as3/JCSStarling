package GameObject
{
	import flash.geom.Point;
	
	import starling.events.Event;

	public class FollowClip extends OneClip
	{
		
		private var target: GameObject = null;
		
		/**
		 * will add on target object and stay for a time and follow target GameObject///
		 * position: start position	(reference target position)						///
		 * name: name of texture or animation										///
		 * type: 1 = texture, 2 = animation											///
		 * last: after this long will remove this, 0 for not delete 				///
		 * target: which objetc it's following										///
		 */
		public function FollowClip(position:Point, name:String, type:int, last:int,target:GameObject)
		{
			this.target = target;
			
			super(position, name, type, time);		// find the OneClip's argument
		}
		
		override protected function update(e:Event):void
		{
			// TODO Auto Generated method stub
			super.update(e);
			
			//follow target position
			this.x = target.x + this.position.x;
			this.y = target.y + this.position.y;
			
		}
		
	}
}