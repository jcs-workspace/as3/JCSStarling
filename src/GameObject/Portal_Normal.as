package GameObject
{
	import starling.events.Event;

	public class Portal_Normal extends GameObject
	{
		public static var instance:Portal_Normal = null;
		private var player:GameObject = null;
		private var lastMap :int = 0;
		private var targetMap:int = 0;
		
		public function Portal_Normal(p :GameObject, lastMap:int, targetMap:int)
		{
			this.player = p;
			this.lastMap = lastMap;
			this.targetMap = targetMap;
			super();
		}
		
		override protected function over(e:Event):void{
			
			super.over(e);
		}
		
		override protected function start(e:Event):void{
			
			this.displayGlobalObject("Portal_01 instance 1",2,4,50);
			
			super.start(e);
		}
		
		override protected function update(e:Event):void{
			
			super.update(e);
		}
		
		public function transferMap():void{
			Main.map_manager.findPlayerSpawnPoint(lastMap,targetMap);
			Main.instance.switchInterface(Main.map_manager.checktMap(targetMap));		// load map
		}
		
	}
}