package GameObject
{
	import starling.events.Event;
	import starling.extensions.fluocode.Fluocam;

	public class Camera extends GameObject
	{
		private static var target:GameObject = null;
		private static const adjustPlayerXPositionInCamera : int = 0;
		private static const adjustPlayerYPositionInCamera : int = -50;
		
		public var speedX:Number = 0;		// this' x coordinate
		public var speedY:Number = 0;		// this' y coordinate
		
		public var camera:Fluocam = null;
		
		// this will store all the ui in this gameobject
		public var cInGameUI :In_Game_User_Interface = null;
		
		private var XtotheEdge:Boolean = false;
		private var YtotheEdge:Boolean = false;
		
		
		/**
		 * target: The Player that this camera are going to fallow,
		 * the x position of this camera
		 * the y position of this camera
		 */
		public function Camera(target:GameObject,x:Number,y:Number)		// constructor
		{
			target = target;
			speedX = x;
			speedY = y;
			
			super();
		}
		
		override protected function over(e:Event):void{					// destructor
			removeChild(camera);
			super.over(e);
		}
		
		override protected function start(e:Event):void{
			// set camera, target: fallowing object, screen width, screen height
			camera = new Fluocam(Main.instance.currentInterface, Starling2DFramework_JCS.SCREEN_WIDTH, Starling2DFramework_JCS.SCREEN_HEIGHT);
			cInGameUI = new In_Game_User_Interface(this);
			
			// set position
			
			// add camera to current stage
			addChild(camera);
			addChild(cInGameUI);
			
			// set camera work
			camera.working(true);
			
			// active target
			//camera.changeTarget(target);
			
			super.start(e);
		}
		
		override protected function update(e:Event):void{
			
			cInGameUI.fFollowCamera(this);		// give UI position to follow
			
			super.update(e);
		}
		
		public function fCameraMovenment(x:Number, y :Number, maxWidth:Number, minWidth:Number, maxHeight:Number, minHeight:Number):void{
			
			// if to the edge than we do not have to check,
			// cuz is already at the edge
			if(!XtotheEdge){																// out of RangeX
				this.x = x + adjustPlayerXPositionInCamera;		// assign to player's x coordinate
				if(x  >= maxWidth){			// check Right-Plane
					this.x = maxWidth + adjustPlayerXPositionInCamera;
					XtotheEdge = true;
				}else if(x  <= minWidth){		// check Left-Plane
					this.x = minWidth + adjustPlayerXPositionInCamera;
					XtotheEdge = true;
				}
			}else{																			// in the RangeX
				if(x <= maxWidth){						// check Right-Plane
					XtotheEdge = false;
				}else if(x >= minWidth){				// check Left-Plane
					XtotheEdge = false;
				}
			}
			
			
			if(!YtotheEdge){																// out of RangeY
				this.y = y + adjustPlayerYPositionInCamera;		// assign to player's y coordinate
				if(y >= maxHeight){						// checkt Bottom-Plane
					this.y = maxHeight + adjustPlayerYPositionInCamera;
					YtotheEdge = true;
				}else if(y <= minHeight){				// checkt Top-Plane
					this.y = minHeight + adjustPlayerYPositionInCamera;
					YtotheEdge = true;
				}
			}else{																			// in the RangeY
				if(y <= maxHeight){						// check Botton-Plane
					YtotheEdge = false;
				}else if(y >= minHeight){				// check Top-Plane
					YtotheEdge = false;
				}
			}
			
			if(target != null && !XtotheEdge && !YtotheEdge){
				changeTarget(target);
			}else{
				changeTarget(this);
			}
		}
		
		/**
		 * work: true for working, false for not working
		 */
		public function working(work:Boolean):void{
			if(camera != null){
				camera.working(work);
			}else{
				trace("Can't find the camera! Please initialize it!");
			}
		}
		
		/**
		 * This function will handle the target we found
		 */
		public function changeTarget(target:GameObject):void{
			if(camera != null && target != null){
				camera.changeTarget(target);
			}else{
				trace("Target unknown!", target);
			}
		}
		
	}// end class
}// end Package



