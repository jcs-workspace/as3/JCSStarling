package GameObject
{
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.ui.Keyboard;
	
	import interfaces.map.Map;
	
	import managers.PlayerStatusManager;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	import starling.extensions.fluocode.Fluocam;

	public class Player extends GameObject
	{
		private var ns:Object = null;
		
		private static const MAX_MOVE_SPEED :Number = 10;		// the maximum speed player can move
		private static const MAX_JUMP_HEIGHT:Number = 10;		// the maximum height player can reach
		private static const JUMP_SPEED:Number = 10;
		private static const GRAVITY:Number = 4.5;			// fall Speed
		
		private static const JUMP_TIME:Number = 12;
		private var jumpTimer:Number = 0;
		
		//-- key
		private var upKey:Boolean = false;
		private var downKey:Boolean = false;
		private var leftKey:Boolean = false;
		private var rightKey:Boolean = false;
		private var jumpKey:Boolean = false;
		
		public var playerCareer:int = 1;		// 1 : worriar, 2: wizard, 3: archer, 4: thief
		public var face:int = -1;		// 1: right, -1: left
		public var playerSM:PlayerStatusManager = null;
		
		//physics
		private var speedX : Number = 0;		// player's x-position
		private var speedY : Number = 0;		// player's y-position
		private var pJumpHeight :Number = 20;
		private var pMoveSpeed:Number = 5.5;
		private var inAir:Boolean = false;		// check if is in air, (jumping time + falling time)
		private var isJumping : Boolean = false;	// check if is jumping
		private var isFalling:Boolean = true;		// check if is falling
		private var leaveHeight:Number = 0;		// detect point
		private var hitHeight:Number = 0;		// detect point
		
		// sounds
		private var pPlayEffectSound:Boolean = false;
		
		// status
		private var isOnLadder :Boolean = false;		// check if player on the ladder or not
		private var isOnRope:Boolean = false;
		private var climbing:Boolean = false;		// weather climbing rope or ladder
		private var isOnPortal:Boolean = false;		// check if player can transfer to other map
		private var transfering:Boolean = false;	// transfer to the next avaliable map
		
		public var canMove:Boolean = true;
		public var canControl :Boolean = true;
		
		private var cam:Fluocam = null;
		private var scroller:Rectangle;
		
		
		/**
		 * Player
		 * c: 1 for warrior, 2 for wizard, 3 for archer, 4 for thief
		 */
		public function Player(c:int)
		{
			playerCareer = c;
			
			super();
		}
		
		override protected function over(e:Event):void{
			removeEventListener(KeyboardEvent.KEY_DOWN, keypressed);
			removeEventListener(KeyboardEvent.KEY_UP,keyreleased);
			//ns.removeEventListener(MouseEvent.MOUSE_DOWN, onDown);
			//ns.removeEventListener(MouseEvent.MOUSE_UP, onUp);
			this.ns.removeEventListener(MouseEvent.CLICK, onClick, false, 0, true);
			this.ns.removeEventListener(MouseEvent.DOUBLE_CLICK, onDoubleClick, false, 0, true);
			super.over(e);
		}
		
		override protected function start(e:Event):void{
			
			scroller = new Rectangle(0, 0, stage.stageWidth, stage.stageHeight);
			
			ns = Starling.current.nativeStage;
			
			//ns.addEventListener(MouseEvent.MOUSE_DOWN, onDown);
			//ns.addEventListener(MouseEvent.MOUSE_UP, onUp);
			this.ns.doubleClickEnabled = true;
			this.ns.mouseChildren=false;
			this.ns.addEventListener(MouseEvent.CLICK, onClick, false, 0, true);
			this.ns.addEventListener(MouseEvent.DOUBLE_CLICK, onDoubleClick, false, 0, true);
			addEventListener(KeyboardEvent.KEY_DOWN,keypressed);
			addEventListener(KeyboardEvent.KEY_UP,keyreleased);
			
			// add player status manager
			playerSM = new PlayerStatusManager(this);
			// default display
			changeDisplay("stand1_"+playerSM.characterCareer+"_", 2, 1, 1,playerCareer);
			
			//cam = new Fluocam(this,1280,720);
			//addChild(cam);
			
			super.start(e);
		}
		
		override protected function update(e:Event):void{
			
			scroller.x = (this.x - scroller.width + 640);
			scroller.y = (this.y - scroller.height + 240);
			//root.scrollRect = scroller;
			
			//movement
			this.x += speedX;
			this.y += speedY;
			
			super.update(e);
			
			if(Main.game_manager.mGameStatus != "SelectCharacter_Interface"){
				physics();
				moveAndStatus();
				climb();
				hitWall();
				transfer();
			}
			
			//help playerSM to update
			playerSM.update();
		}
		
		private function onClick(event: MouseEvent):void{
			//Main.playerChoice = playerChoice;	
			//playerSM.switchStatus("Move");
		}
		
		private function onDoubleClick(event: MouseEvent):void{
			//trace("Runt this?" + playerCareer);
		}
		
		private function keypressed(e:KeyboardEvent):void{
			if(e.keyCode == Keyboard.UP){
				upKey = true;
			}else if(e.keyCode == Keyboard.DOWN){
				downKey = true;
			}else if(e.keyCode == Keyboard.LEFT){
				leftKey = true;
			}else if(e.keyCode == Keyboard.RIGHT){
				rightKey = true;
			}else if(e.keyCode == Keyboard.E){
				if(!inAir) pPlaySoundEffect(0);	// playJumpSound
				jumpKey = true;
			}
		}
		
		private function keyreleased(e:KeyboardEvent):void{
			if(e.keyCode == Keyboard.UP){
				upKey = false;
			}else if(e.keyCode == Keyboard.DOWN){
				downKey = false;
			}else if(e.keyCode == Keyboard.LEFT){
				leftKey = false;
			}else if(e.keyCode == Keyboard.RIGHT){
				rightKey = false;
			}else if(e.keyCode == Keyboard.E){
				jumpKey = false;
			}
		}
		
		/**
		 * move left and right
		 * also change player facing direction
		 */
		private function moveAndStatus():void{
			
			if(!climbing){		// while is not climbing anything
				if(upKey && !isFalling){
					speedX = 0; speedY = 0;		// we do not want to move while we transfer
					playerSM.switchStatus("Stand");
				}else if(rightKey){
					face = 1;
					if(inAir){ pMoveSpeed = 4.8; }
					else{ pMoveSpeed = 5.5; }
					if(!inAir){ speedX = pMoveSpeed; }
					if(!isFalling) playerSM.switchStatus("Walk");
				}else if(leftKey){
					face = -1;
					if(inAir){ pMoveSpeed = 4.8; }
					else{ pMoveSpeed = 5.5; }
					if(!inAir){ speedX = -pMoveSpeed; }
					if(!isFalling) playerSM.switchStatus("Walk");
				}else if(downKey && !inAir){
					speedX = 0; speedY = 0;		// we do not want to move while we Prone
					playerSM.switchStatus("Prone");
				}else{
					speedX = 0;
					if(speedX == 0 && speedY == 0 && !inAir){
						playerSM.switchStatus("Stand");
					}
				}
			}
			
			if(jumpKey && !isJumping && !isFalling){
				inAir = true;			// leave the ground
				isJumping = true;		// start jumping
			}
		}
		
		/**
		 * use to simulate phsycal effects
		 */ 
		private function physics():void{
			// jumping handler and falling handler
			onJumpAndFall();
			
			//land on platform
			landPlatform();
		}
		
		/**
		 * handle jump movenment and fall movenment
		 */
		private function onJumpAndFall():void{
			// falling
			if(isFalling){
				if(!climbing) playerSM.switchStatus("Jump");
				if(speedY < GRAVITY){
					speedY += GRAVITY;
				}
				//if falling too fast, limit its speed
				else{
					speedY = GRAVITY;
				}
			}else speedY = 0;	// end isFalling
			
			//trace("inAir: " + inAir);
			//trace("isJumping: " + isJumping);
			//trace("isFalling: " + isFalling);
			
			// jumping
			if(isJumping){
				//playerSM.switchStatus("Jump");
				jumpTimer++;
				if(jumpTimer < JUMP_TIME/2) speedY = -pJumpHeight;
				else{
					isJumping = false;
					jumpTimer = 0;
					//isFalling = true;		// break throught the if statement
				}
			}// end isJumping
			
		}// end function
		
		/**
		 * make player landing on floor
		 * if player leave floor, let him fall
		 */
		private function landPlatform():void{
			//this point predict the center bottom point of next frame
			//to change how much place player will stand on, change this rectangle
			var predictRect:Rectangle = new Rectangle(x+0.2*width +speedX , y+height-2 +speedY , 0.6*width, 2);
			var detectPoint:Point = new Point(x+width/2,y+height+2);
			var isdetect:Boolean = false;//detect if anything under player
			
			
			for each(var object:DisplayObject in Map.instance.collidableObjects){
				// dectect anything
				if(pointCollision(detectPoint,object)){
					// Hit ground moment
					if(isFalling || isJumping){
						// record current heigth
						hitHeight = y;
						
						// play sound : hit ground
						//Main.audioManager.playsound("");
						
						var compare:Number = hitHeight - leaveHeight;
						//if height enought, -- health
						if(compare > 200){
							//var point:Point = new Point();
							// -- health
						}
					}
					
					//already in the platform
					if(!colideWith(object)){		// debug
						inAir = false;
						isFalling = false;
						isJumping = false;
						isdetect = true;
					}else{
						isdetect = true;		// set isdetect equals to true
					}
				}// end if
			}// end for
			
			//landing on the colidable, prevent flash
			for each(var _object:DisplayObject in Map.instance.collidableObjects){
				if(rectCollision(predictRect,_object)){
					//if already colide with a floor don't try to fix it
					if(!colideWith(_object)){
						speedY = _object.y - y -height;
					}
				}
			}
			
			// if not on the platform, start falling
			if(!isdetect && !isJumping){
				// leave ground moment
				if(!isFalling || isOnLadder || isOnRope){
					//record current height
					leaveHeight = y;
				}
				isFalling = true;		// start going down
				inAir = true;
				//isJumping = false;		// end jump start going down
			}
			
		}
		
		/**
		 * move down and up with rope and ladder
		 */
		private function climb():void{
			// set default to false
			isOnLadder = false;
			var ladder :DisplayObject = null;
			// player mush go to the center of the ladder to stand
			// this range means how close the player must get the ladder
			var onRange:Number = 70;
			
			// check if player can use a ladder
			for each(var object:DisplayObject in Map.instance.ladders){
				if(x+width/2 > object.x+object.width/2 - onRange &&
					x+width/2 < object.x+object.width/2 + onRange &&
					y+height > object.y){
					isOnLadder = true;
					ladder = object;// this become the ladder that play can use
				}
			}
			
			// how close need to use
			var useRange:Number = 50;
			// on ladder player can go up and down
			// will not fall
			if(isOnLadder)		// check if there are avaliable ladder could climb
			{
				// player can stand on the ladder
				speedY = 0;
				if(upKey){
					climbing = true;
					this.x = ladder.x;
				}
			}else{
				climbing = false;
			}
			
			
			if(climbing){			// if we are climbing do these action instead
				if(rightKey && jumpKey || rightKey && jumpKey && upKey){
					pPlaySoundEffect(0);
					speedX = pMoveSpeed;
					isOnLadder = false;
					climbing = false;
				}else if(leftKey && jumpKey || leftKey && jumpKey && upKey){
					pPlaySoundEffect(0);
					speedX = -pMoveSpeed;
					isOnLadder = false;
					climbing = false;
				}else if(upKey){
					playerSM.switchStatus("Ladder");
					this.currentAnim.play();
					speedY = -pMoveSpeed;
				}else if(downKey){
					this.currentAnim.play();
					speedY = pMoveSpeed;
				}else{
					if(this.currentAnim.currentFrame == 0) this.currentAnim.currentFrame = 0;			// adjust the framerate while no climbing
					else if(this.currentAnim.currentFrame == 1) this.currentAnim.currentFrame = 1;
				}
				
			}
			
		}// end function
		
		/**
		 * prevent player from go out the screen
		 */
		private function hitWall():void{
			if(x + speedX <= Main.instance.current_MapMinWidth - (Starling2DFramework_JCS.SCREEN_WIDTH/2)) {		// detect Left
				x = Main.instance.current_MapMinWidth - (Starling2DFramework_JCS.SCREEN_WIDTH/2);
			}
			else if (x + width >= Main.instance.current_MapMaxWidth + (Starling2DFramework_JCS.SCREEN_WIDTH/2)) {	// detect Right
				x = Main.instance.current_MapMaxWidth - width + (Starling2DFramework_JCS.SCREEN_WIDTH/2);
			}
		}
		
		/**
		 * This will check what portal is this then displayed
		 */
		private function transfer():void{
			isOnPortal = false;
			var portal:Portal_Normal = null;
			
			var onRange:Number = 25;
			
			for each(var object:Portal_Normal in Map.instance.portals){
				if(x+width/2 > object.x+object.width/2 - onRange &&
					x+width/2 < object.x+object.width/2 + onRange &&
					y+height > object.y){
					isOnPortal = true;
					portal = object;
				}
			}
			
			// check is start transfer or not
			var startTransfer:Boolean = false;
			
			if(isOnPortal && !isOnLadder && !isOnRope && !startTransfer){
				if(upKey){
					startTransfer = true;
				}
			}
			
			if(startTransfer && !transfering){
				// transfer to next avaliable map
				pPlaySoundEffect(1);
				portal.transferMap();
				transfering = true;
			}
			
		}// end function
		
		/**
		 * handle the death for player
		 */
		public function death():void{
			
		}
		
		
	}// end class
}// end package