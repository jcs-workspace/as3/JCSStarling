package GameObject
{
	import flash.geom.Point;
	
	import starling.events.Event;

	public class OneClip extends GameObject
	{
		public static const FADE_TIME:int = 20;
		
		/**
		 * set this to true can fade out when destroy
		 */
		public var isFadeOut:Boolean = false;
		
		/**
		 * set this to true can fade out when generated
		 */
		public var isFadeIn:Boolean = false;
		
		protected var position :Point = null;
		protected var name:String = null;
		protected var time:int = 0;				// time the effect will be
		protected var type:int = 1;
		private var timer:int = 0;
		
		/**
		 * this class will add on target object and stay for a time ///
		 * position: start position									///
		 * name: name of texture or animation						///
		 * type: 1 = texture, 2 = animation							///
		 * last: after this long will remove this, 0 for not delete ///
		 */
		public function OneClip(position:Point,name:String,type:int,time:int)
		{
			this.position = position;
			this.name = name;
			this.type = type;
			this.time = time;
			
			super();
		}
		
		override protected function start(e:Event):void{
			
			this.x = position.x; this.y = position.y;		// asssgin position to the target (GameObject)
			
			if(name != null) changeDisplay(name, type);
			
			if(isFadeIn){
				this.alpha = 0;
				this.alphaChange(FADE_TIME, type);
			}
			
			super.start(e);
		}
		
		override protected function update(e:Event):void{
			
			//fade out function
			if(isFadeOut && timer + FADE_TIME >= time){
				this.alphaChange(FADE_TIME,0);
			}
			
			if(time != 0){
				timer ++;
				if(timer >= time){
					this.dispose();
					this.parent.removeChild(this);
				}
			}
			
			super.update(e);
		}
	}
}