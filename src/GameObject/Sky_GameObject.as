package GameObject
{
	import starling.display.BlendMode;
	import starling.display.Image;

	public class Sky_GameObject extends GameObject
	{
		private var sky1:Image;
		private var sky2:Image;
		private var movespeed:Number = 0.5;
		
		public function Sky_GameObject(mapName: String)
		{
			sky1 = Main.map_assets.findImage(mapName, 2);
			sky1.blendMode = BlendMode.NONE;
			addChild(sky1);
			
			sky2 = Main.map_assets.findImage(mapName, 2);
			sky2.blendMode = BlendMode.NONE;
			sky2.x = -1280;
			addChild(sky2);
		}
		
		public function update():void{
			sky1.x += movespeed;
			if(sky1.x == 1280){
				sky1.x = -1280;
			}
			
			sky2.x += movespeed;
			if(sky2.x == 1280){
				sky2.x = -1280;
			}
		}
		
		
	}
}