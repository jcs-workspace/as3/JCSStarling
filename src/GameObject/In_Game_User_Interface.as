package GameObject
{
	import starling.core.Starling;
	import starling.display.Image;
	import starling.events.Event;

	public class In_Game_User_Interface extends GameObject
	{
		private static var trackObject:GameObject = null;
		private static var instance: In_Game_User_Interface = null;
		
		private var ns:Object = null;		// create native object
		
		protected var title_image:Image = null;
		protected var cHealthBar:GameObject = null;
		protected var cManaBar :GameObject = null;
		
		
		public function In_Game_User_Interface(target:GameObject)
		{
			trackObject = target;		// assign Target
			
			super();
		}
		
		
		override protected function over(e:Event):void{
			
			super.over(e);
		}
		
		override protected function start(e:Event):void{
			instance = this;
			
			ns = Starling.current.nativeStage;
			
			this.ns.doubleClickEnabled = true;
			this.ns.mouseChildren=false;
			
			super.start(e);
		}
		
		override protected function update(e:Event):void{
			
			super.update(e);
		}
		
		/**
		 * this will track x position and y position of the target
		 */
		public function fFollowCamera(target:GameObject):void{
			if(target != null){
				this.x = target.x;		// track coordinate x
				this.y = target.y;		// track coordinate y
			}
		}
		
	}// end class
}// end Package