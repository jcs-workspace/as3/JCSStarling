package managers
{
	import flash.geom.Point;
	
	import interfaces.map.Novice_Market;
	import interfaces.map.Novice_Road_01;
	import interfaces.map.Novice_Village;

	public class Map_Manager
	{
		public static var instance:Map_Manager = null;
		
		/**
		 * Position player will spawn
		 */
		public var currentPlayerSpawnPosition:Point = new Point(400,500);
		public var adjustPlayerPositionX:Number = 0;
		public var adjustPlayerPositionY:Number = 150;
		
		public function Map_Manager()
		{
			instance = this;
			
			super();
		}
		
		/**
		 * check map id,
		 * then return map name.
		 */
		public function checktMap(mapID:int):String{
			if(mapID == 940001) return "Novice_Village";
			else if(mapID == 940002) return "Novice_Road_01";
			else if(mapID == 940003) return "Novice_Market";
			
			return null;
		}
		
		/**
		 * Assign player position for the next map,
		 * so we do not have to send player position to the Database
		 */
		public function findPlayerSpawnPoint(lastMap:int, targetMap:int):void{
			if(lastMap == 940001 && targetMap == 940002){				// Novice_Village to Novice_Road_01
				currentPlayerSpawnPosition.x = Novice_Road_01.toNovice_Village_Position.x;
				currentPlayerSpawnPosition.y = Novice_Road_01.toNovice_Village_Position.y + adjustPlayerPositionY;
			}else if(lastMap == 940001 && targetMap == 940003){			// Novice_Village to Novice_Market
				currentPlayerSpawnPosition.x = Novice_Market.toNovice_Village_Position.x;
				currentPlayerSpawnPosition.y = Novice_Market.toNovice_Village_Position.y + adjustPlayerPositionY;
			}else if(lastMap == 940002 && targetMap == 940001){			// Novice_Road_01 to Novice_Village
				currentPlayerSpawnPosition.x = Novice_Village.toNovice_Road_01_Position.x;
				currentPlayerSpawnPosition.y = Novice_Village.toNovice_Road_01_Position.y + adjustPlayerPositionY;
			}else if(lastMap == 940003 && targetMap == 940001){			// Novice_Market to Novice_Village
				currentPlayerSpawnPosition.x = Novice_Village.toNovice_Market_Position.x;
				currentPlayerSpawnPosition.y = Novice_Village.toNovice_Market_Position.y + adjustPlayerPositionY;
			}
		}
		
	}
}