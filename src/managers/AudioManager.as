package managers
{
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;

	public class AudioManager
	{
		public static var MUTE:Boolean = false;			// mute Boolean
		public static var instance:AudioManager = null;		// instance
		
		public var currentBGM_volumeController:SoundChannel = null;
		
		private var mysong:Sound = new Sound();
		
		public function AudioManager()
		{
			instance = this;	
		}
		
		/**
		 * find music by given name and play the sound at a new channels
		 */
		public function playsound(name:String,volume:Number = 1,loop:int = 1):SoundChannel{
			if(!MUTE){
				var req:URLRequest = new URLRequest("audios/"+name+".mp3");		// allow mp3 format
				var sound:Sound = new Sound(req);
				var stransform:SoundTransform = new SoundTransform(volume);
				var channel :SoundChannel = sound.play(0,loop,stransform);
				
				if(channel != null) 
					return channel;
				else return new SoundChannel();
			}
			else return new SoundChannel();
		}
		
		/**
		 * this function, in order to let us set bgm volume Globally
		 */
		public function setVolume(volume:Number):void{
			this.currentBGM_volumeController.soundTransform.volume = volume;
		}
	}
}