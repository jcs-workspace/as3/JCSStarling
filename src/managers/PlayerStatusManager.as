package managers
{
	import flash.geom.Point;
	import flash.media.SoundChannel;
	
	import GameObject.FollowClip;
	import GameObject.Player;

	public class PlayerStatusManager
	{
		private static var EFFECTED_TIME:int = 60 * 5;		// the time the enemy skill effect
		
		public static var instance:PlayerStatusManager = null;
		
		private var player:Player = null;
		public var status:String = "Stand";		// players status
		public var characterCareer :String = "warrior";
		
		private var playerEffectStatus : int = 0;
		private var effectedTimer:int = 0;
		
		public var currentEffect:FollowClip = null;
		
		//private var s_move:SoundChannel = null;
		//private var s_climb:SoundChannel = null;
		private var s_normal_attack:SoundChannel = null;
		private var s_death:SoundChannel = null;
		
		public function PlayerStatusManager(p:Player)
		{
			player = p;		// assign player to player
			
			instance = this;
			
			// assign career, in order to find the correct texture
			if(player.playerCareer == 1) characterCareer = Main.instance.characterCareer1;
			else if(player.playerCareer == 2)characterCareer = Main.instance.characterCareer2;
			else if(player.playerCareer == 3)characterCareer = Main.instance.characterCareer3;
			else if(player.playerCareer == 4)characterCareer = Main.instance.characterCareer4;
			
			//new sound channel instance
			s_normal_attack = new SoundChannel();
			s_death = new SoundChannel();
			
			//new effect instance
			currentEffect = null;
			
			super();
		}
		
		public function switchStatus(news:String):void{
			//tell sound manager to play sound
			soundManage(news);
			
			status = news;
			
			// find text depends on 4 main careers (characterCareer)
			switch(news){
				case "Stand":
					player.changeDisplay("stand1_"+characterCareer+"_",2,1,1,player.playerCareer);
					player.changeDisplayDirection(player.face);
					break;
				case "Alert":
					player.changeDisplay("alert_"+characterCareer,2,1);
					player.changeDisplayDirection(player.face);
					break;
				case "Walk":
					player.changeDisplay("walk1_"+characterCareer,2,1,4,player.playerCareer);
					player.changeDisplayDirection(player.face);
					break;
				case "Prone":
					player.changeDisplay("prone_"+characterCareer+"_",2,1,1,player.playerCareer);
					player.changeDisplayDirection(player.face);
					player.changeDisplayPositionY(10);
					break;
				case "Jump":
					player.changeDisplay("jump_"+characterCareer+"_",2,1,1,player.playerCareer);
					player.changeDisplayDirection(player.face);
					break;
				case "Ladder":
					player.changeDisplay("ladder_"+characterCareer+"_",2,1,4,player.playerCareer);
					player.changeDisplayDirection(player.face);
					break;
				case "Rope":
					player.changeDisplay("rope_"+characterCareer+"_",2,1,2,player.playerCareer);
					player.changeDisplayDirection(player.face);
					break;
				case "Normal_Attack":
					player.changeDisplay("Normal_Attack_"+characterCareer+"_",2,1);
					player.changeDisplayDirection(player.face);
					break;
				case "Death":
					player.changeDisplay("Death_"+characterCareer+"_",2,1);
					player.changeDisplayDirection(player.face);
					break;
				case "Fly":
					player.changeDisplay("fly_"+characterCareer+"_",2,1,5,player.playerCareer);
					player.changeDisplayDirection(player.face);
					break;
				default:
					// since the image have the same naming, 
					// we just have to print unknown status
					trace("unknow status:",news);
					break;
			}
		}
		
		/**
		 * return current status
		 */
		public function getStatus():String{
			return status;
		}
		
		/**
		 * manage how sound play for player
		 */
		private function soundManage(s:String):void{
			//attend to attack 
			if(s == "Normal_Attack"){
				//if not already move
				if(status != "Normal_Attack"){
					s_normal_attack = Main.audioManager.playsound("Sound_Move.mp3",1,3);
				}
				
				s_death.stop();
			}
			else if(s == "Death"){		// attend to death
				if(status != "Death"){
					s_death = Main.audioManager.playsound("Sound_Climb.mp3",1,3);
				}
				s_normal_attack.stop();
			}
			else{		// stop every audio if no movenment
				s_normal_attack.stop();
				s_death.stop();
			}
		}
		
		/**
		 * set effected status, auto change animation according to effected type
		 */
		public function effectedInput(type:int):void{
			playerEffectStatus = type;
			
			//add effect for player effected effect type
			var p:Point = new Point(0,0);
			switch(type){
				// normal
				case 0:
					Main.instance.currentInterface.removeChild(currentEffect);
					break;
				// Toxic
				case 1:
					currentEffect = new FollowClip(p, "Status_Ghost", 2, EFFECTED_TIME, this.player);
					currentEffect.isFadeOut = true;					
					Main.instance.currentInterface.addChild(currentEffect);
					break;
				// Weak
				case 2:
					currentEffect = new FollowClip(p, "Status_Ghost", 2, EFFECTED_TIME, this.player);
					currentEffect.isFadeOut = true;					
					Main.instance.currentInterface.addChild(currentEffect);
					break;
				default:
					trace("Unknown Status:", type);
					break;
			}
		}
		
		/**
		 * playerSM doesn't has it own update function,
		 * this update() will be called by its player instance
		 */
		public function update():void{
			//check if any status
			if(playerEffectStatus != 0){
				//count time
				effectedTimer ++;
				//if powerup over, reset status
				if(effectedTimer >= EFFECTED_TIME){
					effectedTimer = 0;
					playerEffectStatus = 0;
				}
			}
		}
		
		/**
		 * playerEffectStatus
		 * 0: normal (no status follow)
		 * 1: Toxic
		 * 2: Weak
		 */
		public function getEffectedStatus():int{
			return playerEffectStatus;
		}
		
	}
}