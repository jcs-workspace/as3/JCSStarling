package 
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import starling.core.Starling;
	import starling.extensions.fluocode.Fluocam;
	
	[SWF(width="1280",height="720",frameRate="60",backgroundColor="0x000000")]
	public class Starling2DFramework_JCS extends Sprite
	{
		public static var _starling:Starling = null;
		public static var instance : Starling2DFramework_JCS = null;
		
		public static const SCREEN_WIDTH:int = 1280;
		public static const SCREEN_HEIGHT:int = 720;
		
		public var camera:Rectangle = null;
		public var camera_position:Point = new Point(0,0);
		
		
		public function Starling2DFramework_JCS()
		{
			instance = this;
			
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.SHOW_ALL;
			
			// set camera
			camera = new Rectangle(camera_position.x,camera_position.y,SCREEN_WIDTH,SCREEN_HEIGHT);
			
			Starling.handleLostContext = true;
			_starling = new Starling(Main, stage);
			
			stage.color = 0x000000;
			_starling.stage.color = 0x000000;		// white: 0xFFFFFF
			
			//display status
			_starling.showStats = true;				// show the status of the Game (FrameRate, MEM, DRW)
			_starling.antiAliasing = 1;
			_starling.start();
		}
	}
}