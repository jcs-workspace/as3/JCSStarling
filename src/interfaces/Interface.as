package interfaces
{
	import starling.events.Event;

	public class Interface extends GameObject
	{
		/**
		 * declare type, 1: for image, 2: animation
		 */
		protected const IMAGE_TYPE:int = 1;
		protected const ANIMATION_TYPE:int = 2;
		
		/**
		 * find correct assets
		 */
		protected const CHARACTER_ASSETS:int = 1; protected const EFFECTS_ASSETS:int = 2;
		protected const ITEMS_ASSETS:int = 3; protected const MAP_ASSETS:int = 4;
		protected const MOB_ASSETS:int = 5; protected const SKILLS_ASSETS:int = 6;
		protected const STRING_ASSETS:int = 7; protected const UI_ASSETS:int = 8;
		
		public function Interface()
		{
			super();
		}
		
		/**
		 * layout image and animation on the stage
		 */
		protected function layout():void{
			
			
		}
		
		//interface gameobject will automatically layout when added on stage
		override protected function start(e:Event):void{
			layout();
			super.start(e);
		}
	}
}