package interfaces.map
{
	import flash.geom.Point;
	import flash.ui.Keyboard;
	
	import GameObject.Camera;
	import GameObject.Player;
	import GameObject.Portal_Normal;
	import GameObject.Sky_GameObject;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.KeyboardEvent;

	public class Novice_Village extends Map
	{
		public static const MAP_ID:int = 940001;
		
		public static var instance:Novice_Village = null;
		//private var sky_gameObject:Sky_GameObject = null;
		
		//-- Portal Postion Set--//
		public static const toNovice_Market_Position:Point = new Point(600, 350);		// this portal position are going to Novice_Market
		public static const toNovice_Road_01_Position:Point = new Point(100, 350);		// this portal postion are going to Novice_Road_01
		
		public function Novice_Village(career:int)
		{
			mPlayerCareer = career;
			// Width Range
			Main.instance.current_MapMaxWidth = nMapMaxWidth = 520;		// Right
			Main.instance.current_MapMinWidth = nMapMinWidth = 0;		// left
			// Heigth Range
			Main.instance.current_MapMaxHeight = nMapMaxHeight = 800;		// Bottom
			Main.instance.current_MapMinHeight = nMapMinHeight = 0;			// Top
			
			collidableObjects = null;
			super();
		}
		
		override protected function over(e:Event):void{
			if(collidableObjects) collidableObjects = null;
			if(ladders) ladders = null;
			if(ropes) ropes = null;
			if(portals) portals = null;
			if(camera) camera = null;
			if(sky_gameObject) sky_gameObject = null;
			super.over(e);
		}
		
		override protected function start(e:Event):void{
			instance = this;
			touchable = false;
			//Starling2DFramework_JCS._starling.simulateMultitouch = true;
			
			// set background music
			Main.instance.setBGM("/Map/Novice_Village/Novice_Village", 1);
			
			collidableObjects = new Vector.<DisplayObject>();
			ladders = new Vector.<DisplayObject>();
			ropes = new Vector.<DisplayObject>();
			portals = new Vector.<Portal_Normal>();
			
			super.start(e);		//start layout()
			
			//-- Add Player depend on Manin.instance.playerChoice --//
			//-- add Camera follow the object
			if(mPlayerCareer == WARRIOR_ID){ Main.currentClientCharacter = new Player(WARRIOR_ID); }
			else if(mPlayerCareer == WIZARD_ID){ Main.currentClientCharacter = new Player(WIZARD_ID); }
			else if(mPlayerCareer == ARCHER_ID){ Main.currentClientCharacter = new Player(ARCHER_ID); }
			else if(mPlayerCareer == THIEF_ID){ Main.currentClientCharacter = new Player(THIEF_ID); }
			
			Main.currentClientCharacter.x = Main.map_manager.currentPlayerSpawnPosition.x; 
			Main.currentClientCharacter.y = Main.map_manager.currentPlayerSpawnPosition.y;
			//Main.currentClientCharacter.pivotX = Main.currentClientCharacter.width * 0.5;
			addChild(Main.currentClientCharacter);
			
			//-- init Camera 
			camera = new Camera(Main.currentClientCharacter, 0, 0);
			addChild(camera);
			
			// change target to follow
			camera.changeTarget(Main.currentClientCharacter);
			
		}
		
		override protected function layout():void{
			// find assets (GetComponent in Unity)
			sky_gameObject = new Sky_GameObject("Novice_Village_Sky");
			var ground : Image = Main.map_assets.findImage("Ground", 1); collidableObjects.push(ground);
			var ladder1:Image = Main.map_assets.findImage("Ladder ", 1); ladders.push(ladder1);
			var toNovice_Market_Portal :Portal_Normal= new Portal_Normal(Main.currentClientCharacter,
																		MAP_ID,
																		Novice_Market.MAP_ID);		// to Novice_Market
			portals.push(toNovice_Market_Portal);
			var toNovice_Road_01_Portal:Portal_Normal = new Portal_Normal(Main.currentClientCharacter,
																		MAP_ID,		// last map
																		Novice_Road_01.MAP_ID);		// target map
			portals.push(toNovice_Road_01_Portal);
			
			// declare position
			ground.x = 0; ground.y = 600; ground.scaleX = 0.5; ground.scaleY = 0.5;
			ladder1.x = 0; ladder1.y = 100; ladder1.scaleX = 0.5; ladder1.scaleY = 0.5;
			toNovice_Market_Portal.x = toNovice_Market_Position.x; toNovice_Market_Portal.y = toNovice_Market_Position.y;
			toNovice_Road_01_Portal.x = toNovice_Road_01_Position.x; toNovice_Road_01_Portal.y = toNovice_Road_01_Position.y;
			
			// add on to stage
			addChild(sky_gameObject);
			addChild(ground);
			addChild(ladder1);
			addChild(toNovice_Market_Portal);
			addChild(toNovice_Road_01_Portal);
			
			super.layout();
		}
		
		override protected function update(e:Event):void{
			
			sky_gameObject.update();			// make sure the sky moves
			camera.fCameraMovenment(Main.currentClientCharacter.x, 		// make sure the camera moves
									Main.currentClientCharacter.y,
									nMapMaxWidth,		// right
									nMapMinWidth,		// left
									nMapMaxHeight,		// bottom
									nMapMinHeight);		// top
			
			super.update(e);
		}
		
		override protected function keyreleased(e:KeyboardEvent):void{
			
			super.keyreleased(e);
		}
		
		override protected function keypressed(e:KeyboardEvent):void{
			if(e.keyCode == Keyboard.F1){
				
			}else if(e.keyCode == Keyboard.F2){
				
			}else if(e.keyCode == Keyboard.F3){
				
			}else if(e.keyCode == Keyboard.F4){
				
			}
			
			super.keypressed(e);
		}
		
		override protected function sendCharacterMapID(career:int, mapId:int):void{
			super.sendCharacterMapID(career,mapId);
		}
		
	}// end class
}// end package