package interfaces.map 
{
	import flash.geom.Point;
	import flash.ui.Keyboard;
	
	import GameObject.Camera;
	import GameObject.Player;
	import GameObject.Portal_Normal;
	import GameObject.Sky_GameObject;
	
	import interfaces.World_Interface;
	
	import starling.display.DisplayObject;
	import starling.events.Event;
	import starling.events.KeyboardEvent;

	public class Map extends World_Interface
	{
		public static const MAP_ID : int = 000000;		// none
		public static var instance:Map = null;
		
		// Scene Object only animationed but not interactable
		protected var sky_gameObject:Sky_GameObject = null;
		
		//-- Collusion Objects --//	interactable!
		public var collidableObjects : Vector.<DisplayObject> = null;
		public var portals : Vector.<Portal_Normal> = null;
		public var ladders:Vector.<DisplayObject> = null;		// chek ladder obejcts avaliable to climb
		public var ropes:Vector.<DisplayObject> = null;		// check rope objects avaliable to climb
		
		protected var currentMapID:int = 000000;
		protected var currentCharacter:int = 0;
		
		// Player in this Client
		protected var mPlayerCareer:int = 0;
		
		//-- Career ID
		protected static const WARRIOR_ID :int = 1;
		protected static const WIZARD_ID :int = 2;
		protected static const ARCHER_ID:int = 3;
		protected static const THIEF_ID:int = 4;
		
		//-- since the game are MO we will like to spawn only 4 player in a map
		protected var warrior:Player = null;
		protected var wizard:Player =null;
		protected var archer:Player = null;
		protected var thief:Player = null;
		
		//-- Camera
		//protected var camera:Fluocam = null;
		protected var camera:Camera = null;
		//-- Player's camera could not go through this range
		protected var nMapMaxWidth:Number = 0;		// this will store every map's width for camera movenment
		protected var nMapMinWidth:Number = 0;		
		protected var nMapMaxHeight:Number = 0;		// this will store every map's height for camera movenment
		protected var nMapMinHeight:Number = 0;		
		
		public function Map()
		{
			instance = this;
			
			super();
		}
		
		override protected function over(e:Event):void{
			removeEventListener(KeyboardEvent.KEY_DOWN,keypressed);
			removeEventListener(KeyboardEvent.KEY_UP,keyreleased);
			super.over(e);
		}
		
		override protected function start(e:Event):void{
			
			// add keys
			addEventListener(KeyboardEvent.KEY_DOWN,keypressed);
			addEventListener(KeyboardEvent.KEY_UP,keyreleased);
			
			super.start(e);
		}
		
		override protected function layout():void{
			
			super.layout();
		}
		
		override protected function update(e:Event):void{
			
			super.update(e);
		}
		
		
		protected function fCameraControl(p:Player,position:Point = null):void{
			if(p != null){
				
			}
		}
		
		protected function sendCharacterMapID(career:int, mapId:int):void{
			this.currentMapID = mapId;
			this.currentCharacter = career;
			
			// we will send message to server, in order to let server change data from data base
			
			super.sendCharacterMapID(career, mapId);
		}
		
		protected function keyreleased(e:KeyboardEvent):void{
			
		}
		
		protected function keypressed(e:KeyboardEvent):void{
			if(Main.DEVELOPER_USED){
				if(e.keyCode == Keyboard.ENTER){
					Main.instance.switchInterface("Login_Interface");
				}
			}// end if
		}
		
	}// end class
}// end package