package interfaces.map
{
	import flash.geom.Point;
	
	import GameObject.Camera;
	import GameObject.Player;
	import GameObject.Portal_Normal;
	import GameObject.Sky_GameObject;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.KeyboardEvent;

	public class Novice_Market extends Map
	{
		public static const MAP_ID:int = 940003;
		public static var instance:Novice_Market = null;
		
		// this portal are going to Novice_Village,
		// and the position in this map!!!
		public static const toNovice_Village_Position :Point = new Point(600,350);
		
		
		
		public function Novice_Market(career:int)
		{
			mPlayerCareer = career;
			
			// Width Range
			Main.instance.current_MapMaxWidth = nMapMaxWidth = 1000;		// Right
			Main.instance.current_MapMinWidth = nMapMinWidth = -1920;		// left
			// Heigth Range
			Main.instance.current_MapMaxHeight = nMapMaxHeight = 800;		// Bottom
			Main.instance.current_MapMinHeight = nMapMinHeight = 0;			// Top
			
			super();
		}
		
		override protected function over(e:Event):void{
			if(collidableObjects) collidableObjects = null;
			if(ladders) ladders = null;
			if(ropes) ropes = null;
			if(portals) portals = null;
			if(camera) camera = null;
			if(sky_gameObject) sky_gameObject = null;
			super.over(e);
		}
		
		override protected function start(e:Event):void{
			instance = this;
			
			Main.instance.setBGM("/Map/Novice_Market/Novice_Market");
			
			collidableObjects = new Vector.<DisplayObject>();
			ladders = new Vector.<DisplayObject>();
			ropes = new Vector.<DisplayObject>();
			portals = new Vector.<Portal_Normal>();
			
			super.start(e);
			
			//-- Add Player depend on Manin.instance.playerChoice --//
			if(mPlayerCareer == WARRIOR_ID){ Main.currentClientCharacter = new Player(WARRIOR_ID); }
			else if(mPlayerCareer == WIZARD_ID){ Main.currentClientCharacter = new Player(WIZARD_ID); }
			else if(mPlayerCareer == ARCHER_ID){ Main.currentClientCharacter = new Player(ARCHER_ID); }
			else if(mPlayerCareer == THIEF_ID){ Main.currentClientCharacter = new Player(THIEF_ID); }
			
			Main.currentClientCharacter.x = Main.map_manager.currentPlayerSpawnPosition.x; 
			Main.currentClientCharacter.y = Main.map_manager.currentPlayerSpawnPosition.y;
			addChild(Main.currentClientCharacter);
			
			//-- init Camera 
			camera = new Camera(Main.currentClientCharacter, 0, 0);
			addChild(camera);
			
			// change target to follow
			camera.changeTarget(Main.currentClientCharacter);
		}
		
		override protected function layout():void{
			
			sky_gameObject = new Sky_GameObject("Novice_Market_Sky");
			var ground : Image = Main.map_assets.findImage("Ground", 1); collidableObjects.push(ground);
			var toNovice_Village_Portal :Portal_Normal= new Portal_Normal(Main.currentClientCharacter,
																			MAP_ID,
																			Novice_Village.MAP_ID);
			portals.push(toNovice_Village_Portal);
			
			// set position
			ground.x = 0; ground.y = 600; ground.scaleX = 0.5; ground.scaleY = 0.5;
			toNovice_Village_Portal.x = toNovice_Village_Position.x; toNovice_Village_Portal.y = toNovice_Village_Position.y;
			
			// add child
			addChild(sky_gameObject);
			addChild(ground);
			addChild(toNovice_Village_Portal);
			
			super.layout();
		}
		
		override protected function update(e:Event):void{
			
			sky_gameObject.update();			// make sure the sky moves
			camera.fCameraMovenment(Main.currentClientCharacter.x, 		// make sure the camera moves
									Main.currentClientCharacter.y,
									nMapMaxWidth,		// right
									nMapMinWidth,		// left
									nMapMaxHeight,		// bottom
									nMapMinHeight);		// top
			
			super.update(e);
		}
		
		override protected function keyreleased(e:KeyboardEvent):void{
			
			super.keyreleased(e);
		}
		
		override protected function keypressed(e:KeyboardEvent):void{
			
			super.keypressed(e);
		}
		
	}// end class
}// end package