package interfaces
{
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	
	import GameObject.Player;
	import GameObject.Sky_GameObject;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	
	public class SelectCharacter_Interface extends Interface
	{
		public static var instance:SelectCharacter_Interface = null;
		private var ns:Object = null;
		private var playerChoice:int = 0;	// 0: none, 1: warrior, 2: wizard, 3: archer, 4: thief
		private var skyObject:Sky_GameObject = null;		// sky as GameObject
		
		// key
		private var enterKey:Boolean = false;
		
		// position in Select Character menu
		private static const pWarriorPosition:Point = new Point(500,500);
		private static const pWizradPoisition:Point = new Point(600,500);
		private static const pArcherPosition:Point = new Point(700,500);
		private static const pThiefPosition:Point = new Point(800,500);
		
		/**
		 * depend on playerChoice, 
		 * we decide what client could control!
		 */
		public static var warrior:Player = null;
		public static var wizard:Player = null;
		public static var archer:Player = null;
		public static var thief:Player = null;
		
		private var qKey:Boolean = false;
		private var wKey:Boolean = false;
		private var eKey:Boolean = false;
		private var rKey:Boolean = false;
		
		
		public function SelectCharacter_Interface()
		{
			super();
		}
		
		override protected function over(e:Event):void{
			//-- remove listener
			//this.ns.removeEventListener(MouseEvent.CLICK, onClick, false, 0, true);
			//this.ns.removeEventListener(MouseEvent.DOUBLE_CLICK, onDoubleClick, false, 0, true);
			removeEventListener(KeyboardEvent.KEY_DOWN,keypressed);
			removeEventListener(KeyboardEvent.KEY_UP,keyreleased);
			ns = null;
			super.over(e);
		}
		
		override protected function start(e:Event):void{
			instance = this;
			
			ns = Starling.current.nativeStage;
			
			//key listner
			addEventListener(KeyboardEvent.KEY_DOWN,keypressed);
			addEventListener(KeyboardEvent.KEY_UP,keyreleased);
			this.ns.doubleClickEnabled = true;
			this.ns.mouseChildren=false;
			//this.ns.addEventListener(MouseEvent.CLICK, onClick, false, 0, true);
			//this.ns.addEventListener(MouseEvent.DOUBLE_CLICK, onDoubleClick, false, 0, true);
			
			// must layout first
			super.start(e);
			
			warrior = new Player(1);
			//wizard = new Player(2);
			//archer = new Player(3);
			thief = new Player(4);
			
			// set position
			warrior.x = pWarriorPosition.x; warrior.y = pWarriorPosition.y;
			//wizard.x = pWizradPoisition.x; wizard.y = pWizradPoisition.y;
			//archer.x = pArcherPosition.x; archer.y = pArcherPosition.y;
			thief.x = pThiefPosition.x; thief.y = pThiefPosition.y;
			
			// add on to stage
			addChild(warrior);
			//addChild(wizard);
			//addChild(archer);
			addChild(thief);
			
		}
		
		override protected function layout():void{
			
			skyObject = new Sky_GameObject("Novice_Village_Sky");
			addChild(skyObject);
			
			super.layout();
		}
		
		override protected function update(e:Event):void{
			skyObject.update();
			
			onSelectMovenment();
			
			onSelectKey();
			
			super.update(e);
		}
		
		private function onSelectKey():void{
			if(qKey && playerChoice != 1){
				pPlaySoundEffect(1);
				playerChoice = 1;		// select warrior
			}else if(wKey && playerChoice != 2){
				pPlaySoundEffect(1);
				playerChoice = 2;		// select wizard
			}else if(eKey && playerChoice != 3){
				pPlaySoundEffect(1);
				playerChoice = 3;		// select archer
			}else if(rKey && playerChoice != 4){
				pPlaySoundEffect(1);
				playerChoice = 4;		// select thief
			}
		}
		
		/**
		 * Selecting player play differernt animation and sound
		 */
		private function onSelectMovenment():void{
			if(warrior != null /*&& wizard != null && archer != null*/ && thief != null){
				if(playerChoice == 1){
					warrior.playerSM.switchStatus("Walk");
					//wizard.playerSM.switchStatus("Stand");
					//archer.playerSM.switchStatus("Stand");
					thief.playerSM.switchStatus("Stand");
				}else if(playerChoice == 2){
					warrior.playerSM.switchStatus("Stand");
					wizard.playerSM.switchStatus("Walk");
					archer.playerSM.switchStatus("Stand");
					thief.playerSM.switchStatus("Stand");
				}else if(playerChoice == 3){
					warrior.playerSM.switchStatus("Stand");
					wizard.playerSM.switchStatus("Stand");
					archer.playerSM.switchStatus("Walk");
					thief.playerSM.switchStatus("Stand");
				}else if(playerChoice == 4){
					warrior.playerSM.switchStatus("Stand");
					//wizard.playerSM.switchStatus("Stand");
					//archer.playerSM.switchStatus("Stand");
					thief.playerSM.switchStatus("Walk");
				}				
			}
			
		}// end function
		
/*		private function onClick(event: MouseEvent):void{
			//Main.playerChoice = playerChoice;	
			//playerSM.switchStatus("Move");
			if(Main.instance.currentInterfaceName == "SelectCharacter_Interface") playerChoice = 4;
		}
		
		private function onDoubleClick(event: MouseEvent):void{
			if(Main.instance.currentInterfaceName == "SelectCharacter_Interface") playerChoice = 1;
		}*/
		
		private function keyreleased(e:KeyboardEvent):void{
			if(e.keyCode == Keyboard.ENTER){
				enterKey = false;
			}else if(e.keyCode == Keyboard.Q){
				qKey = false;
			}else if(e.keyCode == Keyboard.W){
				wKey = false;
			}else if(e.keyCode == Keyboard.E){
				eKey = false;
			}else if(e.keyCode == Keyboard.R){
				rKey =false;
			}
		}
		
		private function keypressed(e:KeyboardEvent):void{
			if(e.keyCode == Keyboard.ENTER){
				Main.instance.playerChoice = getCarreer();		// player have been selected
				// send message to client and check player statys (Pending...)
				if(getCarreer() != 0) Main.instance.switchInterface("Novice_Village");		// load map
				//enterKey = true;
			}else if(e.keyCode == Keyboard.Q){
				qKey = true;
			}else if(e.keyCode == Keyboard.W){
				wKey = true;
			}else if(e.keyCode == Keyboard.E){
				eKey = true;
			}else if(e.keyCode == Keyboard.R){
				rKey = true;
			}
		}
		
		public function getCarreer():int{
			if(playerChoice >= 1 && playerChoice <= 4) {		// protected the value before we return it
				return this.playerChoice;
			}else trace("Character choice should Higher than 1, but lower than 4, Your Choice: ", this.playerChoice);
			
			return 0; // return none by Default
		}
		
		/**
		 * send message to server,
		 * channel: since this is mo game we only have one channel
		 * choice: character choice this character select
		 */
		private function sendMessageToFindCharacterState(choice:int):void{
			// assign to net and packet
		}
		
		
	}
}