package interfaces
{
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	
	import GameObject.Sky_GameObject;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.events.KeyboardEvent;

	public class Login_Interface extends Interface
	{
		public static var instance:Login_Interface = null;
		
		private var tempCharacter:GameObject = null;
		
		//--- GameObjects ---//
		private var skyObject:Sky_GameObject = null;		// sky as GameObject
		private var logo:GameObject = null;					// logo
		private var logoPosition:Point =new Point(-100,-100);
		private var login_board:GameObject = null;			// login board
		private var login_boardPosition:Point =new Point(675,375);
		
		//-- TextField
		private const SHOW_TEXTFIELD:Boolean = false;			// show background, border or not
		private const TEXT_FONT:int = 10;
		private const TEXT_STYLE:String = "Arial";
		private var textFormat:TextFormat = new TextFormat(TEXT_STYLE, TEXT_FONT, 0x000000);
		private var utLoaction:Point = new Point(900, 453);		// username textfield position x, y
		private var ptLoaction:Point = new Point(900, 528);		// password textfield position x, y
		private var uTextField:TextField = null;		// add in layout()
		private var pTextField:TextField = null;		// add in layout()
		
		// contructor
		public function Login_Interface()
		{
			super();
		}
		
		override protected function over(e:Event):void{
			//-- remove child
			Starling.current.nativeOverlay.removeChild(uTextField);
			Starling.current.nativeOverlay.removeChild(pTextField);
			//-- remove listener
			removeEventListener(KeyboardEvent.KEY_DOWN,keypressed);
			removeEventListener(KeyboardEvent.KEY_UP,keyreleased);
			
			super.over(e);
		}
		
		override protected function start(e:Event):void{
			instance = this;
			//touchable = false;
			
			// play audio
			Main.instance.setBGM("/UI/Login_Music");
			
			//key listner
			addEventListener(KeyboardEvent.KEY_DOWN,keypressed);
			addEventListener(KeyboardEvent.KEY_UP,keyreleased);
			
			//must layout first
			super.start(e);
		}
		
		override protected function layout():void{
			
			// add sky
			skyObject = new Sky_GameObject("Novice_Village_Sky");
			addChild(skyObject);
			
			logo = new GameObject();
			logo.changeDisplay("logo", IMAGE_TYPE, UI_ASSETS);
			logo.x = logoPosition.x; logo.y = logoPosition.y;
			addChild(logo);
			logo.scaleChange(10,0.5);
			//logo.scaleBounce(15,3,1.2);
			
			login_board = new GameObject();
			login_board.changeDisplay("login_board", IMAGE_TYPE, UI_ASSETS);
			login_board.x = login_boardPosition.x; login_board.y = login_boardPosition.y;
			addChild(login_board);
			login_board.scaleChange(10,0.8);
			//login_board.scaleBounce(15, 3, 12);
			
			//////// Add Username and Password TextField ////////
			//-- Username TextField --//
			uTextField = new flash.text.TextField();
			uTextField.type = TextFieldType.INPUT;
			uTextField.x = utLoaction.x; uTextField.y = utLoaction.y;			// position 
			uTextField.width = 100; uTextField.height = 15;		// widht and height
			uTextField.scaleX = 2;	uTextField.scaleY = 2;		// scaleX and scaleY
			uTextField.defaultTextFormat = textFormat;
			uTextField.border = SHOW_TEXTFIELD; uTextField.background = SHOW_TEXTFIELD;
			//uTextField.background = 0xFFFFFF;
			Starling.current.nativeOverlay.addChild(uTextField);		// add child
			
			//-- Password TextField --//
			pTextField = new flash.text.TextField();
			pTextField.type = TextFieldType.INPUT;
			pTextField.displayAsPassword = true;
			pTextField.x = ptLoaction.x; pTextField.y = ptLoaction.y;			// position 
			pTextField.width = 100; pTextField.height = 15;		// widht and height
			pTextField.scaleX = 2;	pTextField.scaleY = 2;		// scaleX and scaleY
			pTextField.defaultTextFormat = textFormat;
			pTextField.border = SHOW_TEXTFIELD; pTextField.background = SHOW_TEXTFIELD;
			//pTextField.background = 0xFFFFFF;
			Starling.current.nativeOverlay.addChild(pTextField);		// add child
			
			
			//tempCharacter = new GameObject();
			//tempCharacter.changeDisplay("move_",2,1,10);
			//tempCharacter.x = 500; tempCharacter.y = 500;
			//addChild(tempCharacter);
			
			super.layout();  // TODO Auto Generated method stub
		}
		
		override protected function update(e:Event):void{
			
			skyObject.update();
			
			super.update(e);  // TODO Auto Generated method stub
		}
		
		private function keyreleased(e:KeyboardEvent):void{
			
		}
		
		private function keypressed(e:KeyboardEvent):void{
			if(e.keyCode == Keyboard.ENTER){
				Main.game_manager.mGameStatus = "Selecting_Screen";
				Main.instance.switchInterface("SelectCharacter_Interface");
			}else if(e.keyCode == Keyboard.A){
				logo.alphaChange(100,1);
			}
		}
		
	}
}