package interfaces
{
	import starling.events.Event;

	public class World_Interface extends Interface
	{
		public static var instance:World_Interface = null;
		
		//public static var playerCurrentInWorld :GameObject = Main.currentClientCharacter;	// pending
		
		public function World_Interface()
		{
			super();
		}
		
		override protected function over(e:Event):void{
			
			super.over(e);
		}
		
		override protected function start(e:Event):void{
			instance = this;
			touchable = false;
			
			super.start(e);
		}
		
		override protected function update(e:Event):void{
			
			
			super.update(e);
		}
		
	}
}