package 
{
	import starling.textures.TextureAtlas;
	

	public class Assets
	{
		protected static var animation_frame_rate:int = 14;
		public static var playerPNG :TextureAtlas;
		public static var animDefaultPNG :TextureAtlas;
		
		/**
		 * If there are a tone of image in same sprite we will need to declare his own texture atlas,
		 * else if there is only on image (especially Large Image) we do not have to declare texture atlas
		 */
		
		public function Assets()
		{

			super();
		}
		
	}
}