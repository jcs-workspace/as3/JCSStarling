[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)

# JCSStarling

This is a programming interface layered on top of the [Starling][] framework, transforming the event-driven system into a component-driven system.


<!-- Links -->

[Starling]: https://gamua.com/starling/
